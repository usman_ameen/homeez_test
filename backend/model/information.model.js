const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const Schema = mongoose.Schema
const Information = new Schema(
    {
        XID: {
            type: Number,
            default: 0
        },
        Xtable_Valid: {
            type: Boolean,
            default: true
        },
        X_TableInfo: {
            type: String
        }
    },
    { timestamps: true },
)
Information.plugin(AutoIncrement, {inc_field: 'XID'});
module.exports = mongoose.model('information', Information)
