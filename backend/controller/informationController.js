const mongoose = require('mongoose');
const Information = mongoose.model('information');
const getInfo = require('../model/information.model')

insertRecord = async  (req, res) => {
   var inf = new Information();
   inf.X_TableInfo = req.body.info;
   inf.save((err, doc) => {
      if (!err) {
         return res.status(200).json({ success: true, data: 'data saves success' })
      } else {
         return res.status(400).json({ success: false, error: err })
      }
   });
}

getRecord = async  (req, res) => {
   await getInfo.find({}, (err, infoData) => {
      if (err) {
         return res.status(400).json({ success: false, error: err })
      }
      if (!infoData.length) {
         return res
             .status(404)
             .json({ success: false, error: `Movie not found` })
      }
      return res.status(200).json({ success: true, data: infoData })
   }).catch(err => console.log(err))
}

module.exports = {
   insertRecord,
   getRecord
};
