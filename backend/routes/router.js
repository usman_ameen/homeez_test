const express = require('express')

const Data  = require('../controller/informationController');

const router = express.Router()

router.post('/save', Data.insertRecord)
router.get('/get-data', Data.getRecord)

module.exports = router
