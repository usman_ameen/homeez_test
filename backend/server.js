const dbConnection = require('./model/db');
const express = require('express');
const informationController  = require('./controller/informationController');
const informationRouter  = require('./routes/router');
var app = express();
const bodyParser = require('body-parser')
var cors = require('cors');
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(bodyParser.json())
const PORT = 3000;
console.log('dbConnection ..... !');
app.listen(PORT);
app.get('/', (req, res) => {
    res.send('Hello World!')
})
console.log('API server started on http://localhost:' + PORT + '/api');
app.use('/api', informationRouter);
