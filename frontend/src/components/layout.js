import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Show from '../components/show';
import Index from '../components/home';
import Button from '@material-ui/core/Button';

export default function MainLayout() {
    return (
        <Router>
            <div>
                <ul style={{listStyle:'none',display:'flex', justifyContent:'space-evenly', flexDirection:'row'}}>
                    <li>
                        <Button
                            type="button"
                            fullWidth
                            variant="contained"
                            color="primary"
                            style={{
                                color:'#fff'
                            }}
                        >
                        <Link to="/home"><b style={{
                            color:'#fff'
                        }}>Home</b></Link>
                        </Button>
                    </li>
                    <li>
                        <Button
                            type="button"
                            fullWidth
                            variant="contained"
                            color="primary"
                        >
                            <Link to="/show"><b style={{
                                color:'#fff'
                            }}>Show Table</b></Link>
                        </Button>
                    </li>
                </ul>
                <hr />
                <Switch>
                    <Route exact path="/show">
                        <Show />
                    </Route>
                    <Route path="/">
                        <Index />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}
