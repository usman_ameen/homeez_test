import React, {useEffect, useState} from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import HttpRequest from "../global/HttpRequest";
export default function Form() {
    const [text , setTextData] = useState('');
    useEffect(() => {
        console.log('index page init .. !');
    }, []);
    const handleTextInput = (e) => {
        console.log('e.target.value');
        setTextData(e.target.value);
    }
    const submitForm = () => {
        console.log('going to save data to database ... !');
        console.log(text);
        let formData = {
            info:text,
        };
        HttpRequest.saveRecord(formData).then((response) => {
            console.log('response');
            console.log(response);
            setTextData('');
            alert('Data Saved Successfully .. !');
        }).catch((error) => {
           console.log('error');
           console.log(error);
        });
    }
    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div>
                <form noValidate>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Info"
                        name="email"
                        autoFocus
                        value={text}
                        onChange={handleTextInput}
                    />
                    <Button
                        type="button"
                        fullWidth
                        variant="contained"
                        color="primary"
                        onClick={submitForm}
                    >
                        Submit
                    </Button>
                </form>
            </div>
        </Container>
    );
}
