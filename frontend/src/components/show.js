import React , {useState , useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Config from "../global/Config";
import axios from "axios";
import HttpRequest from "../global/HttpRequest";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

function createData(id, info, valid , createdAt) {
    return {id, info, valid , createdAt};
}

function ShowTable({props}) {
    const classes = useStyles();
    const [rows , setRows] = useState([]);
    useEffect(() => {
        HttpRequest.getRecords().then((response) => {
            const Data = [];
            response.data.data.map((dt) => {
                let validD = 'false';
                if(dt.Xtable_Valid == true) {
                    validD = 'true';
                }
                Data.push(createData(dt.XID,dt.X_TableInfo,validD,dt.createdAt));
            });
            setRows(Data);
        }).catch((error) => {
           console.log('error');
           console.log(error);
        });
    }, []);
    if(rows.length > 0) {
        console.log('rows');
        console.log(rows);
    }
    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Id</TableCell>
                        <TableCell align="center">Info</TableCell>
                        <TableCell align="center">Valid</TableCell>
                        <TableCell align="center">Date</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.id}>
                            <TableCell component="th" scope="row">
                                {row.id}
                            </TableCell>
                            <TableCell align="center">{row.info}</TableCell>
                            <TableCell align="center">{row.valid}</TableCell>
                            <TableCell align="center">{row.createdAt}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default ShowTable;
