/**
 * Re Maintain Structure Config.js 12-04-2020 11:40 PM
 * React Native Learn Releif App
 * Developed By Digital Waves
 **/

import axios from 'axios';
import Config from './Config';

let baseUrlConfig = Config.baseUrl;
export const baseUrl = baseUrlConfig;

export default {
  getRecords() {
    let url = baseUrl+`/get-data`;
    return axios.get(url);
  },

  saveRecord(formData) {
    let headers = new Headers();
    headers.append('Access-Control-Allow-Origin', Config.frontend_url);
    headers.append('Access-Control-Allow-Credentials', 'true');
    let url = baseUrl+`/save`;
    return axios.post(url , formData,{
      headers: headers
    })
  },
};
